function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function concatAll(temp, array){
	if(array.length > 0){
		var s = array.shift();
		return concatAll(temp + s, array);
	}
	return temp;
}
