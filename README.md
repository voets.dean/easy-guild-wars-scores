### EasyGuildWarsScores ###

## Overview ##

The goal of this project is to make visualizations of Guild Wars scores for the game castle clash. This code starts from the Guild Wars Data.txt as produced by WoonTools for Castle Clash v3.0. The goal is to automatically parse these files, store them and create a website with different views on the data.

# Status #
I've put the resulting website online: http://users.telenet.be/EasyGuildWarsScores/ . There is a (really ugly) swing UI to generate the website in (SwingMain) which is exported to a runnable jar. 

# Runnable # 
11/06/2017: I've put a version of the generator online.  
jar: http://users.telenet.be/EasyGuildWarsScores/runnable-egwd/easy-guild-wars-scores.jar
readme: http://users.telenet.be/EasyGuildWarsScores/runnable-egwd/readme.txt

To run it you double click the file. An ugly UI will pop-up asking you to choose an input and output folder. Then you click run and it creates the website in the output folder. Put that online and you're ready. 

The input folder should contain another directory with the name of your guild. In there you need the "Guild War Data.txt" files as you get from WoonTools but renamed to date_position_Guild War Data.txt like 2017-11-06_3_Guild War Data.txt (part after the last underscore doesn't matter).

In my case, the input folder, output folder and runnable jar are in my documents. I select as input folder "Documents/EGWD input".

PS Microsoft.PowerShell.Core\FileSystem::\\MYCLOUD-DEAN\Dean\home\Documents> ls '.\EGWD input\420-United'


    Directory: \\MYCLOUD-DEAN\Dean\home\Documents\EGWD input\420-United


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
------        7/14/2017     23:03           1679 2017-07-09_4_Guild War Data.txt
------         9/5/2017     19:15           1718 2017-09-03_3_Guild War Data.txt
------        7/14/2017     23:03           1700 2017-06-29_2_Guild War Data.txt
------        9/25/2017     23:52           1718 2017-09-24_1_Guild War Data.txt
...

In the output folder you selected you'll see something like:


PS Microsoft.PowerShell.Core\FileSystem::\\MYCLOUD-DEAN\Dean\home\Documents> ls '.\EGWD output'


    Directory: \\MYCLOUD-DEAN\Dean\home\Documents\EGWD output


Mode                LastWriteTime         Length Name
----                -------------         ------ ----
------       10/21/2017     02:16           7412 index.html
------        8/21/2017     21:53            685 style.css
------       10/21/2017     00:44          14065 uihelper.js
------        11/6/2017     22:58         399590 easyguildwarsdata.js

# TODO #
Medium term
- Add graphs, exports of data, other fancy things that we might be able to use html components for.
- Support for generating sites for multiple guilds.

Long term
- Save scores to a database/move away from static content as parsing a json string will not keep working fast.
- Create a program to capture Guild Wars data and upload it to a webservice. EasyGuildWarsScores will then use the webservice to update the site.
- Do the same for visualizing other data from WoonTools.

Suggestions and/or help is always welcome. I'm decent at writing java but my html/css suck and I don't really know how to setup a website.

