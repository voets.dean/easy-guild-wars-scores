package easy.guild.wars;

import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.util.Arrays;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import easy.guild.wars.streamable.GsonProvider;
import easy.guild.wars.streamable.results.AllResults;
import easy.guild.wars.streamable.results.Result;
import easy.guild.wars.streamable.ui.impl.GridColumnDefinitions;
import easy.guild.wars.streamable.ui.impl.ViewDefinitions;

public class HtmlGenerator {
	private static final Logger LOGGER = LoggerFactory.getLogger(HtmlGenerator.class);

	private final File outputFolder = new File("output");
	private final File templateFolder = new File("template");
	private final File egwdFile = new File(templateFolder, "easyguildwarsdata.js");
	private final List<File> templateFilesToProcess = ImmutableList.of(egwdFile);

	public void writeHtml(AllResults container) throws IOException {
		System.out.println("TemplateFolder: " + templateFolder.getAbsolutePath());
		System.out.println("OutputFolder: " + outputFolder.getAbsolutePath());

		String resultsJson = GsonProvider.GSON.toJson(container.toJson());
		String columnsJson = GsonProvider.GSON.toJson(GridColumnDefinitions.instance().toJson());
		String viewsJson = GsonProvider.GSON.toJson(ViewDefinitions.getInstance().toJson());
		String propertiesToCopyToMembersJson = GsonProvider.GSON.toJson(Result.PROPERTIES_TO_COPY_TO_MEMBERS);

		for (File file : outputFolder.listFiles()) {
			clear(file);
		}
		LOGGER.info("Preparing to write HTML for {} Results.", container.size());
		LOGGER.debug("cleared output folder");
		for (File tf : templateFolder.listFiles()) {
			LOGGER.debug("Handling {}", tf.getAbsolutePath());
			File processedFile = new File(outputFolder, tf.getName());
			if (!processedFile.exists()) {
				processedFile.createNewFile();
				LOGGER.debug("Created {}", processedFile.getAbsolutePath());
			}
			if (templateFilesToProcess.contains(tf)) {
				LOGGER.info("Found template file " + tf.getAbsolutePath());
				List<String> lines = Files.readAllLines(tf.toPath());
				lines = lines.stream()
						.map((Function<String, String>) l -> l.replace("$results", resultsJson)
								.replace("$columns", columnsJson).replace("$views", viewsJson)
								.replace("$propertiesToCopyToMembers", propertiesToCopyToMembersJson))
						.collect(Collectors.toList());

				File of = new File(outputFolder, tf.getName());
				Files.deleteIfExists(of.toPath());
				Files.write(of.toPath(), lines);
				LOGGER.info("Successfully wrote template file.");
			} else {
				File of = new File(outputFolder, tf.getName());
				Files.deleteIfExists(of.toPath());
				Files.copy(tf.toPath(), of.toPath());
			}
		}
	}

	private void clear(File f) {
		if (f.isDirectory()) {
			Arrays.stream(f.listFiles()).forEach(this::clear);
		}
		f.delete();
	}
}
