package easy.guild.wars;

import java.io.File;
import java.io.IOException;
import java.io.UncheckedIOException;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.TreeMap;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import easy.guild.wars.domainmodel.Guild;
import easy.guild.wars.streamable.results.AllResults;
import easy.guild.wars.streamable.results.Result;

/**
 * Main class with a runner to parse the guild wars scores and create the
 * website.
 * <p>
 * The input files are the Guild Wars Data.txt files from woontools. We also
 * support a slightly different format with an extra value in the third column
 * to support imports from our excel template.
 * <p>
 * The input files are supposed to be in:
 * <ul>
 * <li>INPUT_FOLDER/guild_name/date_rank_anytstring.txt
 * <li>INPUT_FOLDER/guild_name/excel date_rank_anytstring.txt
 * </ul>
 * With the normal txt files in the normal woontools format and the excel files
 * with an additional third column. See in src/main/resources for examples.
 *
 * @author dean
 */
public class Main implements Runnable {
	private static final String EXCEL_PREFIX = "excel ";
	private static final Logger LOGGER = LoggerFactory.getLogger(Main.class);

	private final Parser parser = new Parser();
	private final HtmlGenerator htmlGenerator;

	private final AllResults resultContainer = new AllResults();

	public Main() {
		htmlGenerator = new HtmlGenerator();
	}

	@Override
	public void run() {
		LOGGER.info("Running easy.guild.wars#Main");

		Map<String, List<File>> inputFiles = collectInputFiles();
		LOGGER.info("Found {} guilds", inputFiles.size());

		for (Entry<String, List<File>> entry : inputFiles.entrySet()) {
			Guild guild = new Guild();
			guild.name = entry.getKey();

			LOGGER.info("Parsing {} input files for {}", entry.getValue().size(), guild.name);
			for (File f : entry.getValue()) {
				try {
					String fileName = f.getName().trim();
					boolean fromExcel = fromExcel(fileName);
					fileName = stripExcelPrefix(fileName);
					int rank = getRankFromFilename(fileName);
					Date date = getGWDateFromFilename(fileName);
					Result result = parser.parseAndEnhance(guild, date, rank, f, fromExcel);
					resultContainer.add(result, guild);
				} catch (IOException e) {
					LOGGER.error("IOException during parsing of " + f.getAbsolutePath(), e);
				} catch (ParseException e) {
					LOGGER.error("Failed to read date from filename: " + f.getName(), e);
				}
			}
		}

		try {
			htmlGenerator.writeHtml(resultContainer);
			LOGGER.info("Successfully written out the website.");
		} catch (IOException e) {
			LOGGER.error("IOException generating HTML.", e);
			throw new UncheckedIOException(e);
		}
	}

	private String stripExcelPrefix(String fileName) {
		if (fromExcel(fileName)) {
			fileName = fileName.substring(EXCEL_PREFIX.length()).trim();
		}
		return fileName;
	}

	private boolean fromExcel(String fileName) {
		boolean fromExcel = fileName.startsWith(EXCEL_PREFIX) && fileName.length() > EXCEL_PREFIX.length();
		return fromExcel;
	}

	private Date getGWDateFromFilename(String filename) throws ParseException {
		String[] split = filename.split("_");
		SimpleDateFormat sdf = new SimpleDateFormat("yyyy-M-dd");
		return sdf.parse(split[0]);
	}

	private int getRankFromFilename(String filename) {
		String[] split = filename.split("_");
		if (split.length >= 2) {
			int rank = split[1].charAt(0) - '0';
			if (rank > 0 && rank <= 5) {
				return rank;
			}
		}
		return -1;
	}

	private Map<String, List<File>> collectInputFiles() {
		Map<String, List<File>> result = new TreeMap<>();
		File inputFolder = new File("input");
		LOGGER.info("Looking in " + inputFolder.getAbsolutePath());
		for (File guildFolder : inputFolder.listFiles()) {
			if (guildFolder.isDirectory()) {
				result.put(guildFolder.getName(), new LinkedList<>());
				for (File f : guildFolder.listFiles()) {
					result.get(guildFolder.getName()).add(f);
				}
			}
		}
		return result;
	}
}
