package easy.guild.wars.streamable.results;

import easy.guild.wars.domainmodel.Guild;
import easy.guild.wars.streamable.util.StreamableMap;

public class AllResults extends StreamableMap<GuildResultContainer> {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	public void add(Result result, Guild guild) {
		if (!containsKey(guild.name)) {
			put(guild.name, new GuildResultContainer());
		}
		get(guild.name).add(result);
	}

}
