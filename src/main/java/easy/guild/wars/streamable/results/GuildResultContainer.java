package easy.guild.wars.streamable.results;

import easy.guild.wars.streamable.Streamable;
import easy.guild.wars.streamable.util.StreamableList;

public class GuildResultContainer extends StreamableList<Result> implements Streamable {

	private static final long serialVersionUID = 1L;

	@Override
	public Object toJson() {
		sort((a, b) -> b.date.compareTo(a.date));
		return super.toJson();
	}
}
