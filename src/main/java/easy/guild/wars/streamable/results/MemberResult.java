package easy.guild.wars.streamable.results;

import java.util.Map;
import java.util.TreeMap;

import easy.guild.wars.domainmodel.BasicMemberResult;
import easy.guild.wars.streamable.Streamable;
import easy.guild.wars.streamable.ui.impl.GridColumnDefinitions;

public class MemberResult extends BasicMemberResult implements Streamable {
	public int avgMightAttacked;
	public int targetAvgMight;
	public int targetPoints;

	public int getPointsGainedLost() {
		return points - targetPoints;
	}

	public double getPercent() {
		return points * 100d / targetPoints;
	}

	public static MemberResult enhance(BasicMemberResult bs) {
		MemberResult result = new MemberResult();
		result.account = bs.account;
		result.might = bs.might;
		result.points = bs.points;
		return result;
	}

	@Override
	public Object toJson() {
		Map<String, Object> map = new TreeMap<>();
		map.put(GridColumnDefinitions.NAME.getName(), account.name);
		map.put(GridColumnDefinitions.MIGHT.getName(), might);
		map.put(GridColumnDefinitions.POINTS.getName(), points);
		map.put(GridColumnDefinitions.TARGET_POINTS.getName(), targetPoints);
		map.put(GridColumnDefinitions.PERCENT.getName(), Math.round(getPercent() * 100) / 100d);
		map.put(GridColumnDefinitions.GAINED_LOST.getName(), getPointsGainedLost());
		map.put(GridColumnDefinitions.AVG_ATTACKED.getName(), avgMightAttacked);
		return map;
	}
}
