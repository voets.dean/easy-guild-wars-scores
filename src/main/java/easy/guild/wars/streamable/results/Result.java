package easy.guild.wars.streamable.results;

import java.util.Calendar;
import java.util.Date;
import java.util.List;
import java.util.Locale;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import com.google.common.collect.ImmutableList;

import easy.guild.wars.domainmodel.BasicMemberResult;
import easy.guild.wars.domainmodel.BasicResult;
import easy.guild.wars.domainmodel.Guild;
import easy.guild.wars.streamable.Streamable;
import easy.guild.wars.streamable.util.StreamableMap;

@SuppressWarnings("serial")
public class Result extends StreamableMap<MemberResult> implements Streamable {
	private static final Logger LOGGER = LoggerFactory.getLogger(Result.class);
	private static final double TARGET_RATIO_MAX_MIGHT_OLD = 0.9;
	private static final double TARGET_RATIO_MIN_MIGHT_OLD = 0.8;
	private static final double TARGET_RATIO_MAX_MIGHT = 0.8;
	private static final double TARGET_RATIO_MIN_MIGHT = 0.7;

	public static final List<String> PROPERTIES_TO_COPY_TO_MEMBERS = ImmutableList.of("date", "guildName",
			"guildPoints", "guildTargetPoints", "memberResults", "guildPercent", "rank");

	public Guild guild;
	public Date date;
	public int rank;

	public int guildTargetPoints() {
		int targetPoints = 0;
		for (MemberResult result : values()) {
			targetPoints += result.targetPoints;
		}
		return targetPoints;
	}

	public int guildPoints() {
		int points = 0;
		for (MemberResult result : values()) {
			points += result.points;
		}
		return points;
	}

	public double getGuildPercent() {
		return guildPoints() * 100d / guildTargetPoints();
	}

	public static Result enhance(BasicResult basicResult) {
		Result result = new Result();
		result.guild = basicResult.guild;
		result.date = basicResult.date;
		result.rank = basicResult.rank;
		LOGGER.info("Enhancing " + basicResult);

		final int minMight = basicResult.values().stream().map(bmr -> bmr.might).min(Integer::compare).orElse(0);
		final int maxMight = basicResult.values().stream().map(bmr -> bmr.might).max(Integer::compare).orElse(0);

		Calendar c = Calendar.getInstance(Locale.US);
		c.set(2017, Calendar.OCTOBER, 17);
		boolean newFormula = result.date.after(c.getTime());

		Calendar cResult = Calendar.getInstance(Locale.US);
		cResult.setTime(result.date);

		// The update was on GWs day and the ppl who did it before the update have crazy
		// results. We cap the calculated Opp Might for that GWs to 310k (highest normal
		// one had 309k).
		boolean firstGWsOfNewFormula = cResult.get(Calendar.YEAR) == 2017
				&& cResult.get(Calendar.MONTH) == Calendar.OCTOBER && cResult.get(Calendar.DAY_OF_MONTH) == 19;

		for (BasicMemberResult br : basicResult.values()) {
			MemberResult es = MemberResult.enhance(br);
			if (newFormula) {
				es.avgMightAttacked = (int) Math.floor(((es.points / 5 - 100d) - es.might / 800d) * 1200);
				if (firstGWsOfNewFormula) {
					es.avgMightAttacked = Math.min(310000, es.avgMightAttacked);
				}
			} else {
				es.avgMightAttacked = (int) Math
						.floor((es.points / 5d - Math.pow(es.mightDouble(), 0.47) / 1.33) * 700);
			}
			result.put(es.account.name, es);
		}

		List<Integer> avgMights = result.values().stream().map(mr -> mr.avgMightAttacked).sorted()
				.collect(Collectors.toList());
		int thirdAvgMight = avgMights.get(avgMights.size() - 3);

		for (MemberResult es : result.values()) {
			es.targetAvgMight = getTargetAvgMight(thirdAvgMight, es.might, minMight, maxMight, newFormula);
			if (newFormula) {
				es.targetPoints = (int) Math.floor(5 * (100 + es.might / 800d + es.targetAvgMight / 1200d));
			} else {
				es.targetPoints = 5
						* (int) Math.round(Math.pow(es.mightDouble(), 0.47) / 1.33 + es.targetAvgMight / 700d);
			}
		}
		return result;
	}

	private static int getTargetAvgMight(int maxAvgMight, int might, int minMight, int maxMight, boolean newFormula) {
		double r = Integer.valueOf(might - minMight).doubleValue() / (maxMight - minMight);
		// r between 0 and 1, square to shift stronger requirements to higher mights.
		r = r * r;
		double targetMinMight = newFormula ? TARGET_RATIO_MIN_MIGHT : TARGET_RATIO_MIN_MIGHT_OLD;
		double targetMaxMight = newFormula ? TARGET_RATIO_MAX_MIGHT : TARGET_RATIO_MAX_MIGHT_OLD;
		double targetPercent = (1 - r) * targetMinMight + r * targetMaxMight;
		return (int) Math.round(targetPercent * maxAvgMight);
	}

	@Override
	public Object toJson() {
		Map<String, Object> map = new TreeMap<>();
		List<Object> childrenAsJson = values().stream().sorted((a, b) -> Double.compare(b.getPercent(), a.getPercent()))
				.map(emr -> emr.toJson()).collect(Collectors.toList());

		map.put("date", date);
		map.put("guildName", guild.name);
		map.put("guildPoints", guildPoints());
		map.put("guildTargetPoints", guildTargetPoints());
		map.put("memberResults", childrenAsJson);
		map.put("guildPercent", getGuildPercent());
		map.put("rank", rank);
		return map;
	}
}
