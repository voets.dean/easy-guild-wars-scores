package easy.guild.wars.streamable.ui;

import java.util.Optional;

import easy.guild.wars.streamable.Streamable;

public interface GridColumnDefinition extends Streamable {
	String getKey();

	String getName();

	ViewType getType();

	Optional<Integer> getMaxDigitsAfterComma();

	default Optional<String> getCssClass() {
		return Optional.of("grid-column-" + getName());
	}
}