package easy.guild.wars.streamable.ui.impl;

import java.util.Optional;

import easy.guild.wars.streamable.ui.GridColumnDefinition;
import easy.guild.wars.streamable.util.StreamableMap;

/**
 * A Map with the predefined Columns.
 *
 * @author dean
 *
 */
public class GridColumnDefinitions extends StreamableMap<GridColumnDefinition> {

	public static final GridColumnDefinition AVG_ATTACKED = new GridColumnDefinitionImpl("oppMight", "Opp might",
			Optional.empty());
	public static final GridColumnDefinition GAINED_LOST = new GridColumnDefinitionImpl("gainedLost", "Gained/lost",
			Optional.empty());
	public static final GridColumnDefinition PERCENT = new GridColumnDefinitionImpl("percent", "Percent",
			Optional.of(2));
	public static final GridColumnDefinition TARGET_POINTS = new GridColumnDefinitionImpl("target", "Target",
			Optional.empty());
	public static final GridColumnDefinition POINTS = new GridColumnDefinitionImpl("points", "Points",
			Optional.empty());
	public static final GridColumnDefinition MIGHT = new GridColumnDefinitionImpl("might", "Might", Optional.empty());
	public static final GridColumnDefinition NAME = new GridColumnDefinitionImpl("name", "Name", Optional.empty());
	public static final GridColumnDefinition DATE = new GridColumnDefinitionImpl("date", "Date", Optional.empty());

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private GridColumnDefinitions() {
		super();
	}

	public static final GridColumnDefinitions instance() {
		GridColumnDefinitions map = new GridColumnDefinitions();
		map.put(AVG_ATTACKED.getName(), AVG_ATTACKED);
		map.put(GAINED_LOST.getName(), GAINED_LOST);
		map.put(PERCENT.getName(), PERCENT);
		map.put(TARGET_POINTS.getName(), TARGET_POINTS);
		map.put(POINTS.getName(), POINTS);
		map.put(MIGHT.getName(), MIGHT);
		map.put(NAME.getName(), NAME);
		map.put(DATE.getName(), DATE);
		return map;
	}
}