package easy.guild.wars.streamable.ui.impl;

import java.util.Arrays;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;

import easy.guild.wars.streamable.ui.AbstractViewDefinition;
import easy.guild.wars.streamable.ui.GridViewDefinition;
import easy.guild.wars.streamable.ui.ViewType;
import easy.guild.wars.streamable.util.StreamableMap;

public class ViewDefinitions extends StreamableMap<AbstractViewDefinition> {
	/**
	 *
	 */
	private static final long serialVersionUID = 1L;
	public static final GridViewDefinition GUILD_WARS_GRID = new GridViewDefinitionImpl("guildWarsGrid", ViewType.GRID,
			Arrays.asList(GridColumnDefinitions.NAME.getName(), GridColumnDefinitions.MIGHT.getName(),
					GridColumnDefinitions.POINTS.getName(), GridColumnDefinitions.TARGET_POINTS.getName(),
					GridColumnDefinitions.GAINED_LOST.getName(), GridColumnDefinitions.PERCENT.getName(),
					GridColumnDefinitions.AVG_ATTACKED.getName()));

	private ViewDefinitions() {
		super();
	}

	public static ViewDefinitions getInstance() {
		ViewDefinitions map = new ViewDefinitions();
		map.put(GUILD_WARS_GRID.getName(), GUILD_WARS_GRID);
		return map;
	}
}

class GridViewDefinitionImpl extends ViewDefinitionBaseImpl implements GridViewDefinition {

	private final List<String> columnKeys;

	GridViewDefinitionImpl(String name, ViewType type, List<String> columnKeys) {
		super(name, type);
		this.columnKeys = columnKeys;
	}

	@Override
	public List<String> getColumnNames() {
		return columnKeys;
	}

	@Override
	public Object toJson() {
		Map<String, Object> map = new TreeMap<>();
		map.put("name", getName());
		map.put("type", getType().name());
		map.put("columnKeys", columnKeys);
		return map;
	}

}