package easy.guild.wars.streamable.util;

import java.util.Collection;
import java.util.LinkedList;

import com.google.common.collect.Lists;

import easy.guild.wars.streamable.Streamable;

public class StreamableList<T extends Streamable> extends LinkedList<T> implements Streamable {

	public StreamableList() {
		super();
	}

	public StreamableList(Collection<T> initialValues) {
		super(initialValues);
	}

	@Override
	public Object toJson() {
		return Lists.transform(this, s -> s.toJson());
	}

}
