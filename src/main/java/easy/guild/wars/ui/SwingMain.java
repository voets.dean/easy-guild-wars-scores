package easy.guild.wars.ui;

import java.awt.Color;
import java.util.concurrent.Executors;

import javax.swing.Box;
import javax.swing.BoxLayout;
import javax.swing.JFrame;
import javax.swing.JLabel;
import javax.swing.SwingUtilities;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import easy.guild.wars.Main;

public class SwingMain extends Box implements Runnable {

	/**
	 *
	 */
	private static final long serialVersionUID = 1L;

	private static final Logger LOGGER = LoggerFactory.getLogger(SwingMain.class);

	private final JLabel status = new JLabel("--");

	public SwingMain() {
		super(BoxLayout.PAGE_AXIS);
		this.setSize(800, 600);
		this.setBackground(new Color(200, 200, 200));
		this.setName("EasyGuildWarsScores website generator");

		add(status);

		Executors.newSingleThreadExecutor().submit(this);
	}

	public static void main(String[] args) {
		// Schedule a job for the event-dispatching thread:
		// creating and showing this application's GUI.
		SwingUtilities.invokeLater(new Runnable() {
			@Override
			public void run() {
				// Create and set up the window.
				JFrame frame = new JFrame("Easy Guild Wars Scores");
				frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
				frame.setSize(800, 600);

				SwingMain swingMain = new SwingMain();
				swingMain.setSize(800, 600);

				frame.setContentPane(swingMain);

				// Display the window.
				frame.pack();
				frame.setVisible(true);
			}
		});
	}

	@Override
	public void run() {
		try {
			SwingUtilities.invokeLater(() -> status.setText("Creating website, this might take a while."));
			Main main = new Main();
			main.run();

			SwingUtilities.invokeLater(() -> status.setText("Done!"));
		} catch (Exception e) {
			LOGGER.error("Exception in SwingMain#run", e);
			SwingUtilities.invokeLater(() -> status.setText("Something went wrong!" + e.getMessage()));
		}
	}
}
