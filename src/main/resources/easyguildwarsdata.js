var results = JSON.parse('$results');
var columns = JSON.parse('$columns');
var views = JSON.parse('$views');
var toCopy = JSON.parse('$propertiesToCopyToMembers');

function getEasyResults(){
	return results;
}

function getEasyViews(){
	return views;
}

function getEasyColumns(){
	return columns;
}

var initialize = function(){
	console.log('Copying properties from Results to their containing MemberResults for ' + this.toCopy);
	$.each(results, function(key,guildResultContainer){
		$.each(guildResultContainer, function(index, result){
			$.each(result.memberResults, function(memberName, memberResult){
				$.each(toCopy, function(indexTC, tc){
					memberResult[tc] = result[tc];
				});
			});
		});
	});
	console.log('Finished copying attributes from results to memberResults.');

	columns['Gained/lost'].cssGenerator = function(gainedLost){
		if(gainedLost >= 0) return "success";
		if(gainedLost >= -250) return "warning";
		return "danger";
	}

	columns['Percent'].cssGenerator = function(percent){
		if(percent >= 100) return "success";
		if(percent >= 90) return "warning";
		return "danger";
	}
};
initialize();
