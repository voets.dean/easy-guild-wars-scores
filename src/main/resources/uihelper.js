// Variables defining what is being shown.
var guildName = '420-United';
var warsFilter;
var playersFilter;
var viewName = 'guildWarsGrid';
var currentTab = 'Results';
var averagesShownResults = 3;
var sortColumn = 'Percent';
var sortInverted = false;

// View of properties and what properties to show in what view.
var results = getEasyResults();
var views = getEasyViews();
var columns = getEasyColumns();

// Instance variables.
var memberResults;
var guildResults;
var lastResult;
var guildMemberNames;
var datesResults;

var GREEN = '#dff0d8';
var YELLOW = '#fcf8e3';
var RED = '#f2dede';

var DARK_GREEN = '#d0e9c6';
var DARK_YELLOW = '#faf2cc';
var DARK_RED = '#ebcccc';

function initGuildResultsDropdown(){
	$("#gw-dates-dropdown").empty();
	var allWarsOfGuild = selectResults(null);
	datesResults = [];
	$.each(allWarsOfGuild, function(_i, gw){
		var dateString = gw.date || 'date unknown';
		var formattedDate = formatDate(dateString);
		datesResults.push(formattedDate);
	    var dropdownItem = '<li role="presentation" class="menu-item"><a class="guildWarSelector" href="#">'+ formattedDate +'</a></li>';
		$("#gw-dates-dropdown").append(dropdownItem);	
		console.log('Added li for each GWs of the guild');
	});
	console.log('Added Guild results to dropdown');
	
	$('.guildWarSelector').click(function(e){
		console.log('Clicked results dropdown. ' + this.text);
		e.preventDefault();
		warsFilter = this.text;			
		updateView();
	});
	console.log('Initialized results dropdown.');
	
	$('#dates-forward').click(function(e){
		if(warsFilter){
			warsFilter = datesResults[datesResults.indexOf(warsFilter) + 1];
			updateView();
		}		
	});
	
	$('#dates-backward').click(function(e){
		if(warsFilter){
			warsFilter = datesResults[datesResults.indexOf(warsFilter) - 1];
			updateView();
		}		
	});
}

function initTabs(){
	$("#menu li a").click(function(){
		console.log('Clicked tab ' + this.text);
		$("#menu li").removeClass("selected");
		$("#menu a:contains(\"" + this.text + "\")").parent().addClass("selected");
		
		currentTab = this.text;
		sortColumn = currentTab === 'Per Member' ? 'Date' : 'Percent';
		sortInverted = false;
		
		toggleVisible();
		updateView();
	});
	console.log('Initialized navigation bar.');
}

function initAveragesController(){
	$("#averagesController li.averagesOption").click(function(){
		var str = this.childNodes[0].text;
		console.log('click on averages controller ' + str);
		if(str === "all"){
			averagesShownResults = 0;
		} else{
			averagesShownResults = parseInt(str);
		}
		console.log('averagesShownResults: ' + averagesShownResults);
		updateView();
	});
	console.log('Initialized averagesController');
}

function initPerMember(){	
	$("#guild-member-dropdown").empty();
	$("#guild-member-dropdown-green").empty();
	$("#guild-member-dropdown-red").empty();
	guildMemberNames = $.map(lastResult.memberResults, (mr, i) => mr['Name']); 
	guildMemberNames.sort((a,b) => a.toUpperCase() < b.toUpperCase() ? -1 : 1);
	
	var greenMap = {};
	var redMap = {};
	
	$.each(guildMemberNames, (_i, name) => {
		greenMap[name] = 0;
		redMap[name] = 0;
	});
	var iResults = 0;
	while(iResults < 5){
		$.each(guildResults[iResults].memberResults, (_i, mr) => {
			var memberName = mr["Name"];
			if(guildMemberNames.indexOf(memberName) >= 0){
				var percent = mr["Percent"];
				if(percent >= 100){
					var greenCount = greenMap[memberName];
					greenCount++;
					greenMap[memberName] = greenCount;
				} else if(percent < 90){
					var redCount = redMap[memberName];
					redCount++;
					redMap[memberName] = redCount;
				}
			}
		});
		iResults++;
	}
	
	$.each(guildMemberNames, (_i, name) => {
	    var dropdownItem = '<li role="presentation" class="menu-item"><a class="guildMemberSelector" href="#">'+ name +'</a></li>';
		$("#guild-member-dropdown").append(dropdownItem);
		
		if(greenMap[name] >= 5){
			$("#guild-member-dropdown-green").append(dropdownItem);
		} else if(redMap[name] >= 2){
			$("#guild-member-dropdown-red").append(dropdownItem);
		}
	});
	console.log('Added li for each current guild member');
	
	$('.guildMemberSelector').click(function(e){
		console.log('Clicked guild member selector. ' + this.text);
		e.preventDefault();
		playersFilter = this.text;
		updateView();
	});
	console.log('Added click listener on guild member selectors.');
	
	playersFilter = guildMemberNames[0];
	console.log('Initialized playersFilter: ' + playersFilter);
		
	$("#guild-member-forward").click(function(){
		if(playersFilter){
			playersFilter = guildMemberNames[guildMemberNames.indexOf(playersFilter) + 1];
			updateView();
		}
	});
		
	$("#guild-member-backward").click(function(){
		if(playersFilter) {
			playersFilter = guildMemberNames[guildMemberNames.indexOf(playersFilter) - 1];
			updateView();
		}
	});
}

/**
 * Called when window loaded.
 * <ul>
 * <li>Adds the GWs to the dropdown.
 * <li>Adds tab change listener.
 * <li>Adds GWs dropdown listener.
 * <li>Adds listener for Averages selector
 * <li>Selects default view/result
 * <li>Calls updateView()
 * 
 * @returns
 */
function onWindowLoaded(){
	initGuildResultsDropdown();
	initTabs();
	initAveragesController();
	
	$(".guild-name-placeholder").text(guildName);

	toggleVisible();
	// Initially select last result of guild.
	guildResults = results[guildName];
	lastResult = guildResults[0];
	warsFilter = formatDate(lastResult.date);
	console.log('Set filter to show last GWs result.');
	
	initPerMember();
	
	updateView();
}

/**
 * Creates the view. Called when changing view (except table sorting).
 * 
 * @returns
 */
function updateView(){
	$("#gw-dates-dropdown-header").text(warsFilter || 'Select result');
	$("#guild-member-dropdown-header").text(playersFilter || 'Select member');
	$("#view-container").empty();
	
	if(currentTab === "About"){
		// Nothing to do.
	} else if(currentTab === "Per Member"){
		var results = selectResults(null);
		memberResults = selectMemberResults(results);
		
		google.charts.load("current", {packages:["corechart", "table"]});
		google.charts.setOnLoadCallback(drawPerMember);		
	} else{
		if(currentTab === "Averages"){
			var results = selectResults(null);
			memberResults = createAveragingMR(results);
		} else{
			var results = selectResults(warsFilter);
			memberResults = selectMemberResults(results);
		}
		
		var correctedSortColumn = sortColumn === 'Date' ? 'date' : sortColumn;
		memberResults.sort((a,b) => (correctedSortColumn === 'Name' ? -1 : 1) * (sortInverted ? -1 : 1)  * (b[correctedSortColumn] > a[correctedSortColumn] ? 1 : -1));
		
		var view = views[viewName];
		var html;
		if(view.type === 'GRID'){
			html = gridHtml(view, memberResults);
		}
		
		if(html){
			$("#view-container").append(html);
			
			$('#view-container table th').click(function(){
				var parts = this.innerHTML.split("  ");
				var colName = parts.length === 1 ? parts[0] : parts[1];
				sortInverted = colName === sortColumn && !sortInverted;
				sortColumn = colName;
				updateView();
			});
			console.log('Should have added clicklistener on table headers.');
		}
	}
}

function drawPerMember(){
	console.log('entering drawPerMember');	
	fillInMemberPlaceholders();
	console.log('Filled in PerMember placeholders');	
	
	drawPerMemberPie();	
	console.log('Added PerMember PieChart');	
	
	drawMemberResultsTable();
	console.log('Added PerMember table');	
}

function fillInMemberPlaceholders(){
	var lastResult = memberResults[0];	
	$(".per-member-name").text(lastResult["Name"]);
	$(".per-member-might").text(formatNumber(lastResult["Might"]));
	var lastPercent = Math.round(lastResult["Percent"]*100)/100;
	$(".per-member-last-result").text(formatNumber(lastResult["Points"]) + " (" + formatNumber(lastPercent) + "%)");
	
	var maxScore = 0;
	var maxScoreMr;
	$.each(memberResults, (_i, mr) => {
		if(mr["Points"] > maxScore){
			maxScore = mr["Points"];
			maxScoreMr = mr;
		}
	});

	$(".per-member-top-score").text(formatNumber(maxScore) + " on " + formatDate(maxScoreMr["date"]));
	$(".per-member-joined").text(formatDate(memberResults[memberResults.length-1].date, "fullDate"));
	
	var might5GWsAgo = memberResults[Math.min(4, memberResults.length - 1)]["Might"];
	var currentMight = memberResults[0]["Might"];
	var mightGained = currentMight - might5GWsAgo;
	$('.per-member-might-gained-past-5').text(formatNumberWithPlus(mightGained));
	
	var gainedLost5 = 0;
	var gainedLostAll = 0;
	
	var longestInGreen = 0;
	var currentInGreen = 0;
	
	$.each(memberResults, (i, mr) => {
		var gl = mr["Gained/lost"];
		gainedLostAll += gl;
		if(i < 5){
			gainedLost5 += gl;
		}
		
		if(gl >= 0){
			currentInGreen++;
			longestInGreen = Math.max(currentInGreen, longestInGreen);
		} else{
			currentInGreen=0;
		}
	});
	$(".per-member-gained-lost-5").text(formatNumberWithPlus(gainedLost5));
	$(".per-member-gained-lost-total").text(formatNumberWithPlus(gainedLostAll));
	$(".per-member-longest-green").text(formatNumber(longestInGreen));
}

function formatNumberWithPlus(n){
	var prefix = n >= 0 ? '+' : '';
	return prefix + formatNumber(n);
}

function formatNumber(number){
	if(typeof(number)==='number')
		return new Intl.NumberFormat().format(number);
	else 
		return number;
}

function formatDate(date){
	if(date){
		var dd = new Date(date);
		return dd.toDateString("en-US");	
	} else {
		return "???";
	}
}

function drawPerMemberPie(){
	var greenCount = 0;
	var orangeCount = 0;
	var redCount = 0;
	
	$.each(memberResults, (_i, mr) => {
		var percent = mr["Percent"];
		if(percent >= 100){
			greenCount++;
		} else if(percent < 90){
			redCount++;
		} else{
			orangeCount++;
		}
	});
	
    var data = google.visualization.arrayToDataTable([
      ['Result', 'Nb of Guild Wars'],
	  ['green', greenCount],
	  ['orange', orangeCount],
	  ['red', redCount]
	]);
    var options = {
      pieHole: 0.4,
      colors:[DARK_GREEN, DARK_YELLOW, DARK_RED],
      backgroundcolor:'#E0E0E0',
      legend:'none',
      pieSliceText: 'value',
      pieSliceTextStyle: {
    	  color: 'black',
      },
    };	
	var chart = new google.visualization.PieChart(document.getElementById('per-member-pie'));
	chart.draw(data, options);
}

function drawMemberResultsTable(){
	var data = new google.visualization.DataTable();
    data.addColumn('date', 'Date');
    data.addColumn('number', 'Might');
    data.addColumn('number', 'Points');
    data.addColumn('number', 'Target');
    data.addColumn('number', 'Gained/Lost');
    data.addColumn('number', 'Percent');
    data.addColumn('number', 'Opp might');
    
    var rows = [];
    $.each(memberResults, (_i,mr) => {
    	var backgroundColor = mr["Percent"] >= 100 ? GREEN : mr["Percent"] < 90 ? RED : YELLOW;    
    	var percent = Math.round(mr["Percent"]*100)/100;
    	rows.push(
        		[
        			{v: new Date(mr["date"]), f: formatDate(new Date(mr["date"]))}, 
        			{v: mr["Might"], f: formatNumber(mr["Might"])}, 
        			{v: mr["Points"], f: formatNumber(mr["Points"])}, 
        			{v: mr["Target"], f: formatNumber(mr["Target"])}, 
        			{v: mr["Gained/lost"], f: '<div style="background-color: ' + backgroundColor +';"><span>' + formatNumberWithPlus(mr["Gained/lost"]) +'</span></div>'}, 
        			{v: mr["Percent"], f: '<div style="background-color: ' + backgroundColor +';"><span>' + formatNumber(percent) + '%' +'</span></div>'}, 
        			{v: mr["Opp might"], f: formatNumber(mr["Opp might"])}
        			]
        		);
    });
    
    data.addRows(rows);
    
    var table = new google.visualization.Table(document.getElementById('per-member-table'));

    table.draw(data, {showRowNumber: false, width: '100%', height: '100%', allowHtml: true});
}

/**
 * Creates the averages for the given results (#results according to
 * averagesShownResults)
 * 
 * @param results
 * @returns Averaged results.
 */
function createAveragingMR(results){
	var resultsCopy = averagesShownResults > 0? results.slice(0, averagesShownResults) : results.slice();
	var mcolumns = $.map(getColumnKeys(), k => columns[k]);
	var listMemberResults = {};
	
	$.each(resultsCopy[0].memberResults, function(_i, mr){
		var mrName = mr['Name'];
		listMemberResults[mrName] = {};
		listMemberResults[mrName]['Name'] = mrName;
		$.each(mcolumns, function(_i2, c){
			var colName = c['name'];
			if(colName !== 'Name'){
				listMemberResults[mrName][colName] = [];
			}
		});  
	});
	
	$.each(resultsCopy, function(_i, res){
		$.each(res.memberResults, function(_i, mr){
			var mrName = mr['Name'];
			if(listMemberResults[mrName]){
				$.each(mcolumns, function(_i2, c){
					var colName = c['name'];
					if(colName !== 'Name'){
						if(!isNaN(mr[colName])){
							listMemberResults[mrName][colName].push(mr[colName]);
						}
					}
				});
			}
		});
	});
	console.log('Collected values for all guild members.');
	
	var mrs = [];
	$.each(listMemberResults, function(name, mr){
		var newMr = {};
		newMr['Name'] = name;
		$.each(mcolumns, function(_i2, c){
			var colName = c['name'];
			if(colName !== 'Name'){
				var list = mr[colName];
				var total = 0;
				$.each(list, (i3,v) => total += v);
				var avg = total/list.length;
				if(columns[colName]["maxDigitsAfterComma"]){
					var dac = parseInt(columns[colName]["maxDigitsAfterComma"]);
					avg =  Math.round(avg * Math.pow(10,dac)) /  Math.pow(10,dac);
				} else{
					avg = Math.round(avg);
				}
				newMr[colName] = avg;
			}
		});
		mrs.push(newMr); 
	});
	console.log('Combined the data.');
	return mrs;
}


// ******** Select results ******** //

/**
 * Returns the results of the guild with the given name or all results if no
 * guildName is given.
 */
function selectGuilds(guildName){
	if(guildName){
		var guildResults = results[guildName];
		if(guildResults){
			return [guildResults];
		}
		return [];
	} else{
		var result = [];	
		$.each(results, (k,g) => result.push(g));
		return result;
	}	
}

/**
 * Returns the results of the given date (warsFilter) or all results if
 * !warsFilter.
 * 
 * @param warsFilter
 * @returns
 */
function selectResults(warsFilter){
	var guilds = selectGuilds(guildName);
	var results = [];
	$.each(guilds, (i, guild) => {
		$.each(guild, (i2, result) => {
			if(!warsFilter || formatDate(result.date) === warsFilter){ 
				results.push(result);
				console.log('Added result ' + result.date);
			}
		});
	});

	if(results.length === 1){
		console.log('Filling in guild details on top for ' + results[0]);
		$(".guild-rank-placeholder").text(formatRank(results[0]["rank"]));
		$(".guild-points-placeholder").text(formatNumber(results[0]["guildPoints"]));
		$(".guild-target-placeholder").text(formatNumber(results[0]["guildTargetPoints"]));
		$(".guild-percent-placeholder").text(formatNumber((Math.round(results[0]["guildPercent"]*10)/10)) + "%");
	}
	
	return results;
}

/**
 * Selects from all guild results the memberresults satisfying playersFilter
 * (normally null)
 * 
 * @param results
 * @returns
 */
function selectMemberResults(results){
	var memberResults = [];
	$.each(results, function(i, result){
		$.each(result.memberResults, (i2, mr) => {
			if(currentTab!=="Per Member" || !playersFilter || getOriginalName(mr['Name']) === getOriginalName(playersFilter)){
				memberResults.push(mr);	
			}
		});
	});
	return memberResults;
}

function getOriginalName(name){
	if(nameChanges[name]){
		return getOriginalName(nameChanges[name]);
	} else{
		return name;
	}
}


// ******** Show grid ******** //
	
function gridHtml(view, memberResults){
	var scolumns = $.map(getColumnKeys(), k => columns[k]);
	var hs = '<th></th>'  + concatAll('', $.map(scolumns, c => {
		var name = c['name'];
		if(name === sortColumn){
			if(!sortInverted)
				name = '+  ' + name;
			else
				name = '-  ' + name;
		}
		return wrap(name,'th', c['cssClass']);
	}));
	
	var headers = wrap(hs, 'tr');
		
	var rows = '';
	$.each(memberResults, (i,mr) => rows += gridRowHtml(i, mr, scolumns, memberResults.length));
	
	var showExplainSwitch = warsFilter===formatDate("2017-10-19") && currentTab==="Results";
	var prefix = showExplainSwitch ? "<p>This is the first GWs with the new formula. Some people have done their attacks before the update (during GWs day) and got much higher scores.<br>To fix this, Opp might for this GWs is capped to 310k.</p>" : "";
	return prefix + wrap(headers+rows, 'table', 'table table-striped table-hover table-responsive table-'+view['name']);	
}

function gridRowHtml(index, memberResult, columns, nbMembers){
	var pos = sortInverted ? (nbMembers) - index : index + 1;
	var html = '<td>' + pos + '</td>' ;
	$.each(columns, function(i,c){
		var n = c['name'];
		if(n === "Date"){
			n = "date";	
		}
		var cssClass = c['cssClass'];
		var d = memberResult[n];
		var m = c['maxDigitsAfterComma'];
		var cssGenerator = c.cssGenerator;
		if(m && !isNaN(d) && !isNaN(m)){
			var power = Math.pow(10,m);
			d = Math.round(d*power)/power;
		}
		var elementCss = '';
		if(cssGenerator){
			elementCss = cssGenerator(d) || '';
		}
		var formattedNumber = n==='Gained/lost' ? formatNumberWithPlus(d) : formatNumber(d);
		html += wrap(formattedNumber,'td', cssClass, elementCss);
	});
	return wrap(html,'tr');
}

/**
 * This is/used to be defined in views, generated in java side. I want to get
 * rid of that.
 * 
 * @returns
 */
function getColumnKeys(){
	if(currentTab === 'Per Member'){
		return ["Date", "Might", "Points", "Target", "Gained/lost", "Percent", "Opp might"];
	} else{
		return ["Name", "Might", "Points", "Target", "Gained/lost", "Percent", "Opp might"];	
	}
}

// ******** Utilities ******** //

function getURLParameter(name) {
  return decodeURIComponent((new RegExp('[?|&]' + name + '=' + '([^&;]+?)(&|#|;|$)').exec(location.search) || [null, ''])[1].replace(/\+/g, '%20')) || null;
}

function concatAll(temp, array){
	if(array.length > 0){
		var s = array.shift();
		return concatAll(temp + s, array);
	}
	return temp;
}

/**
 * Wrap a tag around a string and add the combined css.
 */
function wrap(string, tag, cssTemplate, cssData){
	var css = (cssTemplate || '') + (cssTemplate && cssData ? ' ' : '') + (cssData || '');
	var cssPart = (css ? ' class="'+css+ '"' : '');
	return '<' + tag + cssPart + '>' + string + '</' + tag + '>';
}

function toggleVisible(){
	if(currentTab === "About"){
		console.log('Showing about');
		$(".visibleOnResults").hide();
		$(".visibleOnAverages").hide();
		$(".visibleOnPerMember").hide();
		$(".visibleOnAbout").show();
	} else if(currentTab === "Results"){
		console.log('Showing results');
		$(".visibleOnAbout").hide();
		$(".visibleOnAverages").hide();
		$(".visibleOnPerMember").hide();
		$(".visibleOnResults").show();
	} else if(currentTab === "Per Member"){
		console.log('Showing per member');
		$(".visibleOnAbout").hide();
		$(".visibleOnResults").hide();
		$(".visibleOnAverages").hide();
		$(".visibleOnPerMember").show();
	} else{
		console.log('Showing averages');
		$(".visibleOnAbout").hide();
		$(".visibleOnResults").hide();
		$(".visibleOnPerMember").hide();
		$(".visibleOnAverages").show();
	}
	console.log('Shown/hidden elements for ' + currentTab);
}

function formatRank(rank){
	if(rank === 1)
		return "1st";
	if(rank === 2)
		return "2nd";
	if(rank < 6)
		return rank +"th";
	return "??";
}
