package easy.guild.wars.streamable;

import java.util.Date;
import java.util.UUID;

import org.junit.Before;
import org.junit.Test;

import easy.guild.wars.domainmodel.Account;
import easy.guild.wars.domainmodel.BasicMemberResult;
import easy.guild.wars.domainmodel.BasicResult;
import easy.guild.wars.domainmodel.Guild;
import easy.guild.wars.streamable.results.GuildResultContainer;
import easy.guild.wars.streamable.results.Result;
import easy.guild.wars.streamable.ui.impl.GridColumnDefinitions;
import easy.guild.wars.streamable.ui.impl.ViewDefinitions;

public class TestStreaming {
	private Guild guild;
	private BasicResult br;
	private Result er;
	private GuildResultContainer erc;

	@Before
	public void setupTest() {
		guild = new Guild();
		guild.name = "420";

		br = new BasicResult();
		br.date = new Date();
		br.guild = guild;
		br.rank = (int) Math.floor(5 * Math.random());

		BasicMemberResult bmr = createBasicMemberResult();
		br.put(bmr.account.name, bmr);
		bmr = createBasicMemberResult();
		br.put(bmr.account.name, bmr);

		er = Result.enhance(br);

		erc = new GuildResultContainer();
		erc.add(er);
	}

	@Test
	public void testStreaming() {
		System.out.println(GsonProvider.GSON.toJson(br));
		System.out.println(GsonProvider.GSON.toJson(er));
		System.out.println(GsonProvider.GSON.toJson(erc));
		System.out.println();
		System.out.println(GsonProvider.GSON.toJson(GridColumnDefinitions.instance().toJson()));
		System.out.println();
		System.out.println(GsonProvider.GSON.toJson(ViewDefinitions.getInstance().toJson()));
	}

	private Account createAccount() {
		Account a = new Account();
		a.name = UUID.randomUUID().toString();
		return a;
	}

	private BasicMemberResult createBasicMemberResult() {
		BasicMemberResult bmr = new BasicMemberResult();
		bmr.account = createAccount();
		bmr.might = (int) Math.round(400000d * Math.random());
		;
		bmr.points = (int) Math.round(4000d * Math.random());
		return bmr;
	}

	public void testMemberResult() {

	}

}
